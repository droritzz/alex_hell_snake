#!/usr/bin/env python3
##################################
# two scripts for the user's info output
################################
print("""
Name: Ran Chen
Address: Habanim 35 Herzelia
Telephone: 050-8754522
Age: 24
Faculty: Computer Science
Institution: MLA
Average: 78.5
""")

# same output, user provide the data
name = input("please provide your first and last name: ")
address = input("please provide your address: ")
phone = input("please provide your phone number: ")
age = input("please enter your age: ")
faculty = input("please enter your faculty: ")
institution = input("please enter your institution: ")
average = input("please provide your average grade: ")
print(f"\nName: {name}\nAddress: {address}\nTelephone: {phone}\nAge: {age}\nFaculty: {faculty}\nInstitution: {institution}\nAverage: {average}")