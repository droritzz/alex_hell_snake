#!/usr/bin/env python3
###############################
#grades and factors
###############################

name = str(input("please enter student's first and last name: "))
grade = int(input("please enter student's grade: "))
factor = int(input("please enter the factor: "))
grade_with_factor = float(grade*(1+factor/100))
print(f"Name: {name}")
print("Grade: ", round(grade_with_factor, 1))
print(f"Factor: {factor}")