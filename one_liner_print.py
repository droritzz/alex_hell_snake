#!/usr/bin/env python3
##################################
# two one-line scripts
#################################
print("""
A B
A B C
A B C D
""")
print("""
A, B
A, B, C
A, B, C, D
""")